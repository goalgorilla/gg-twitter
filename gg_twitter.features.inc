<?php
/**
 * @file
 * gg_twitter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gg_twitter_ctools_plugin_api() {
  return array("version" => "1");
}

/**
 * Implements hook_views_api().
 */
function gg_twitter_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function gg_twitter_node_info() {
  $items = array(
    'tweet' => array(
      'name' => t('Tweet'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'twitter_feed' => array(
      'name' => t('Twitter feed'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
